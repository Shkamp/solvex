using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GraphGetter.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace GraphGetter.Controllers
{
    public class GraphController : Controller
    {
        private readonly global::GraphGetterContext _context;

        public GraphController(global::GraphGetterContext context)
        {
            _context = context;
        }

        // GET: Graph
        public async Task<IActionResult> Index()
        {
            return _context.Graphs != null ?
                          View(await _context.Graphs.ToListAsync()) :
                          Problem("Entity set 'GraphGetterContext.Graph'  is null.");
        }


        public async Task<string> GetGraph(int? id)
        {
            if (id == null || _context.Graphs == null)
            {
                return "null";
            }

            var graph = await _context.Graphs
                .FirstOrDefaultAsync(m => m.Id == id);
            if (graph == null)
            {
                return "null";

            }

            return JsonConvert.SerializeObject(graph);
        }

        public async Task<string> GetGraphData(int? id)
        {
            if (id == null || _context.Graphs == null)
            {
                return "null";
            }

            var graph = await _context.Graphs
                .FirstOrDefaultAsync(m => m.Id == id);
            if (graph == null)
            {
                return "null";

            }
            JObject jObject = JObject.FromObject(graph);
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    string s = await client.GetStringAsync(graph.Endpoint);
                    jObject.Add("data", JToken.Parse(s));
                }
                catch (Exception e) {
                    jObject.Add("data", null);

                 }
            }
            return jObject.ToString();
        }


        // GET: Graph/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Graphs == null)
            {
                return NotFound();
            }

            var graph = await _context.Graphs
                .FirstOrDefaultAsync(m => m.Id == id);
            if (graph == null)
            {
                return NotFound();
            }

            return View(graph);
        }

        // GET: Graph/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Graph/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Type,Endpoint")] Graph graph)
        {
            if (ModelState.IsValid)
            {
                _context.Add(graph);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(graph);
        }

        // GET: Graph/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Graphs == null)
            {
                return NotFound();
            }

            var graph = await _context.Graphs.FindAsync(id);
            if (graph == null)
            {
                return NotFound();
            }
            return View(graph);
        }

        // POST: Graph/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Type,Endpoint")] Graph graph)
        {
            if (id != graph.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(graph);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GraphExists(graph.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(graph);
        }

        // GET: Graph/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Graphs == null)
            {
                return NotFound();
            }

            var graph = await _context.Graphs
                .FirstOrDefaultAsync(m => m.Id == id);
            if (graph == null)
            {
                return NotFound();
            }

            return View(graph);
        }

        // POST: Graph/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Graphs == null)
            {
                return Problem("Entity set 'GraphGetterContext.Graph'  is null.");
            }
            var graph = await _context.Graphs.FindAsync(id);
            if (graph != null)
            {
                _context.Graphs.Remove(graph);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool GraphExists(int id)
        {
            return (_context.Graphs?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
