using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using GraphGetter.Models;

    public class GraphGetterContext : DbContext
    {
        public GraphGetterContext (DbContextOptions<GraphGetterContext> options)
            : base(options)
        {
        }

        public DbSet<GraphGetter.Models.Graph>? Graphs { get; set; }

    }
