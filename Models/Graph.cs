using System.ComponentModel.DataAnnotations;

namespace GraphGetter.Models
{
    public class Graph
    {
        public int Id { get; set; }
        [Required]
        public string? Name { get; set; }
        public string? Type { get; set; }
        public string? Endpoint { get; set; }
    }
}