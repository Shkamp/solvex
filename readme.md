## GET: Graph
returns the list of graphs
## GET: Graph/Details/1
returns the graph's details in a view
## GET: Graph/GetGraph/1
returns the graph's details as a raw json
## GET: Graph/GetGraphData/1
returns the graph's details as a raw json with the data from the endpoint included
## GET: Graph/Create
graph creation view
## GET: Graph/Edit/5
graph editing view
## GET: Graph/Delete/5
graph deletion view